#include "SourceB.h"

void q2()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	cout << "Part A:" << endl;

	rc = sqlite3_open("carsDealer.db", &db); //Opening the db

	if (rc) //If there were errors
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return;
	}

	cout << "Liat tries to buy a white Ford:" << endl; //Buy case 1
	if (carPurchase(5, 10, db, zErrMsg))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	cout << "Moses tries to buy a white Mercedes:" << endl; //Buy case 2
	if (carPurchase(1, 19, db, zErrMsg))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	cout << "Josh tries to buy a black Hammer:" << endl; //Buy case 3
	if (carPurchase(12, 15, db, zErrMsg))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	system("pause");
	system("CLS");

	cout << "Part B:" << endl;

	cout << "Sara tries to transfer 100000 to Nisim:" << endl; //Transfer case 1
	if (balanceTransfer(3, 2, 100000, db, zErrMsg))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	cout << "Sapir tries to transfer 120000 to Zafir:" << endl; //Transfer case 2
	if (balanceTransfer(9, 7, 120000, db, zErrMsg))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	system("pause");
	system("CLS");

	sqlite3_close(db);
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int balance = 0;
	int price = 0;
	int available = 0;
	stringstream cmd;

	cmd << "select balance from accounts where id=" << buyerid; //Setting the stringstream to the querry which gets the account's balance
	sqlite3_exec(db, cmd.str().c_str(), getInt, &balance, &zErrMsg);

	cmd.str(string()); //Clearing the previous querry
	cmd << "select price from cars where id=" << carid; //Setting the stringstream to the querry which gets the car's price
	sqlite3_exec(db, cmd.str().c_str(), getInt, &price, &zErrMsg);

	cmd.str(string()); //Clearing the previous querry
	cmd << "select available from cars where id=" << carid; //Setting the stringstream to the querry which gets car's availability
	sqlite3_exec(db, cmd.str().c_str(), getInt, &available, &zErrMsg);

	if (available && balance > price) //If the buyer can purchase the car
	{
		cmd.str(string()); //Clearing the previous querry
		cmd << "update accounts set balance=" << balance - price << " where id=" << buyerid; //Setting the stringstream to the querry which sets the account's balance
		sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);

		cmd.str(string()); //Clearing the previous querry
		cmd << "update cars set available=0 where id=" << carid; //Setting the stringstream to the querry which sets car's availability
		sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
		return true;
	}
	else return false;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int fromBalance = 0;
	int toBalance = 0;
	int rc = 0;
	stringstream cmd;

	cout << "Question 2:" << endl;

	cmd << "select balance from accounts where id=" << from; //Setting the stringstream to the querry which gets the account's balance
	sqlite3_exec(db, cmd.str().c_str(), getInt, &fromBalance, &zErrMsg);

	cmd.str(string()); //Clearing the previous querry
	cmd << "select balance from accounts where id=" << to; //Setting the stringstream to the querry which gets the account's balance
	sqlite3_exec(db, cmd.str().c_str(), getInt, &toBalance, &zErrMsg);

	if (amount > fromBalance) return false;
	cmd.str(string()); //Clearing the previous querry
	cmd << "begin transaction";
	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) return false; //If there were errors

	cmd.str(string()); //Clearing the previous querry
	cmd << "update accounts set balance=" << fromBalance - amount << " where id=" << from;
	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) return false; //If there were errors

	cmd.str(string()); //Clearing the previous querry
	cmd << "update accounts set balance=" << toBalance + amount << " where id=" << to;
	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) return false; //If there were errors

	cmd.str(string()); //Clearing the previous querry
	cmd << "end transaction";
	rc = sqlite3_exec(db, cmd.str().c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) return false; //If there were errors
	
	return true;
}

int getInt(void* notUsed, int argc, char** argv, char** azCol)
{
	int* res = (int*)notUsed;
	*res = atoi(argv[0]); //Setting the int as the result
	return 0;
}
