#include "SourceA.h"

void q1()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;

	cout << "Question 1:" << endl;

	rc = sqlite3_open("FirstPart.db", &db); //Opening the db

	if (rc) //If there were errors
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return;
	}

	cout << "create table people(id INTEGER primary key autoincrement, name TEXT)" << endl;

	rc = sqlite3_exec(db, "create table people(id INTEGER primary key autoincrement, name TEXT)", callback, 0, &zErrMsg); //Creating the table - people

	if (rc != SQLITE_OK) //If there were errors
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	system("Pause");
	system("CLS");

	cout << "insert into people(name) values(Gabi)" << endl;

	rc = sqlite3_exec(db, "insert into people(name) values('Gabi')", callback, 0, &zErrMsg); //Inserting Gabi into people

	if (rc != SQLITE_OK) //If there were errors
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	system("Pause");
	system("CLS");

	cout << "insert into people(name) values(Gadi)" << endl;

	rc = sqlite3_exec(db, "insert into people(name) values('Gadi')", callback, 0, &zErrMsg); //Inserting Gadi into people

	if (rc != SQLITE_OK) //If there were errors
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	system("Pause");
	system("CLS");

	cout << "insert into people(name) values(Gali)" << endl;

	rc = sqlite3_exec(db, "insert into people(name) values('Gali')", callback, 0, &zErrMsg); //Inserting Gali into people

	if (rc != SQLITE_OK) //If there were errors
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	system("Pause");
	system("CLS");

	cout << "update people set name=Galit where id=3" << endl;

	rc = sqlite3_exec(db, "update people set name='Galit' where id=3", callback, 0, &zErrMsg); //Changing Gali's name to Galit in people

	if (rc != SQLITE_OK) //If there were errors
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return;
	}

	system("Pause");
	system("CLS");

	sqlite3_close(db);
}